package com.springboot.firsttryspringbootapp;

import org.springframework.boot.CommandLineRunner; // import de commandLineRunner
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FirstTrySpringbootAppApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(FirstTrySpringbootAppApplication.class, args);
	}

    @Override // parce qu'on utilise la méthode de commandLineRunner
    public void run(String... args) {
        System.out.println("Hello world");
    }

}
